# synthetic-event-data

## Zadání:
Jako zadání jsem si vybral jedno z potenciálních zadání na StatsPerform Pro Forum, což je každoroční konference na téma analytika ve fotbale.

Původní text zadání: "Using event data for training, is it possible to build a realistic game simulator that produces synthetic event data that is indistinguishable from the real thing?
If so, this opens the gateway for longitudinal simulations. For example, you could tweak starting conditions and measure the potential impact across multiple seasons." (link: https://www.statsperform.com/resource/from-the-pro-forum-judging-panel-five-potential-abstract-ideas/)

Tento úkol jsem si rozdělil na dvě části:

1) Generovat "smysluplná" syntetická event data - takové, že sekvence akcí budou navazovat

2) Generovat event data, která budou zohledňovat faktory jako: zúčastněné týmy, hráči, předchozí vývoj utkání.


### Přečetl jsem:
Original GAN paper - https://proceedings.neurips.cc/paper/2014/file/5ca3e9b122f61f8f06494c97b1afccf3-Paper.pdf

TimeGAN paper (time-series GAN) - https://papers.nips.cc/paper/2019/file/c9efe5f26cd17ba6216bbe2a7d26d490-Paper.pdf

RTSGAN paper (real-world time-series GAN) - http://recmind.cn/papers/RTSGAN_icdm21.pdf

Recurrent-conditional GAN - https://arxiv.org/abs/1706.02633

Continuous recurrent GAN - https://arxiv.org/abs/1611.09904


### Dělám tohle:
Na téma syntetické generovaní sekvenčních (time-series) dat toho zatím nebylo moc publikováno a většina vzniklých publikacích se zaměřuje na data ve zdravotnictví nebo podobná dobře strukturovaná data. Pouze RTSGAN se zaměřuje na data o variabilní délce s případnými chybějícími hodnotami, bohužel však autoři RTSGAN ještě nezpřístupnili zdrojový kód. Žádný ze zdrojů také zatím neuvádí žádné řešení pro categorical features, které se ve fotbalových datech hojně objevují, takže celý projekt bude poměrně experimentální.

Prozatím jsem se tedy rozhodl využít TimeGAN, s tím že jsem striktně zkrátil počet událostí v rámci každé sekvence (jendoho utkání), jelikož TimeGAN si nedokáže poradit s různými délkami. Počet features jsem omezil na nezbytné minimum, jen abych dokázal, zda jde vůbec generovat smysluplná data. 

Model jsem trénoval na free datech od StatsBomb (football data provider), které jsem předzpracoval za pomocí knihovny MPL Soccer. K vizualizaci dat jsem si napsal vlastní animátor.

## Data download
Data lze stáhnout spuštěním souboru download_data_statsbomb.py, který data zároveň předzpracuje.

Data důležitá pro tento projekt se následně budou nacházet ve složce event-raw - event data z jednotlivých zápasů.

## Spuštění
V notebooku FootGAN.ipynb se trénuje model - první chystám data do požadovaného formátu a následně se trénuje TimeGAN. Následně lze generovat syntetické sekvence za použití tohoto modelu.

Výsledný (prozatimní) model je uložen v souboru synthetizer.pkl, ze kterého lze případně načíst pro generaci syntetických sekvencí bez nutnosti znovu trénovat model.


V notebooku football_viz.ipynb lze spustit nebo uložit animaci z načtených event dat pro jednotlivý zápas (ať už syntetická nebo reálná data)
## Ukázka
Ukázky vizualizací reálného a syntetického utkání lze shlédnout pomocí GIFů - animation_real a animation_synthetic
